alert("hellow")


//	1. Arrow functions Expression
//	Before ES6, function expression syntax: 
function name(parameter){
	statements
}
//Function declaration
//name - function name
//parameters - name of an argument to be passed, placeholder
//statements - ano ung ibabalik niya kay function, function body
//example of normal function
function generateFullName(firstName,middleInitial,lastName){
	return firstName + ' ' + middleInitial + ' ' + lastName;
}



//in es6, arrow function expression, eto ung syntax
const variableName = () => {
	statements
}

//pwede rin eto
(parameters) => {
	statements
}
//example of arrow f expression
const generateFullName(firstName,middleInitial,lastName){
	return firstName + ' ' + middleInitial + ' ' + lastName;
}
/*Additional in arrow function, curly brackets can be omitted but  still can return a value*/

//shorthand
const add = (x,y) => x + y

//longer version
const add = (x,y) => {
	return x + y;
}
//NOTE: when do we use arrow function
//it can be used when you do not needn to  name a function
//the function has a simple task to do
//
//we will use the normal function
let numbers = [1,2,3,4,5];

let numbersMap = numbers.map(function(number){
	return number * number;
})
console.log(numbersMap);

const numbersMap = numbers.map(number => number * number);
console.log(numbersMap);

//template literals
//are strings literals allowing embedded expression (concatination)
const generateFullName = (firstName,middleInitial,lastName) =>{
	return firstName + ' ' + middleInitial + ' ' + lastName;
}

//syntax:
`string text`
`string text line 1
string text line 2`
`string text ${expression} string text`
//template literals can contain placeholders. we used dollar sign  and curly brackets. the expressions in the placeholders  and the text  between the backticks get passed to a function
//concatination
//`${expression}, ${expression}`
//
console.log(`string text line 1
			string text line 2`)


const generateFullName = (firstName,middleInitial,lastName) =>{
	return `${firstName} ${middleInitial}. ${lastName}`
}
//can also do computations or evaluations inside:

`${ 8 * 6 }`

const add = (x,y) => `The sum is ${x+y}`;
//3. DESTRUCTURING ARRAYS AND OBJECTS
//Destructuring assignments syntax  is a js  expression  that makes it possible to unpack values from arrays, or  properties from objects into distinct variables

//normal array
let num = [10,20];
let num = `${num[0]} ${num[1]}`;

//destructuring assignments
let a,b,rest;
[a,b] = [10,20];

console.log(a);
console.log(b);

[a,b, ...rest] = [10,20,30,40,50];
console.log(rest)

//example, we have an array  that contains  the name  of a person . If we  want to combine it to a full name , we will need to do it like this:

let name = ["Juan", "Dela"m "Cruz"];
let fullName = `${name[0]} ${name[1]} ${name[2]}`; //eto ung normal

//destructuring assignments
let firstName,middleName,lastName ;
[firstName,middleName,lastName] = ["Juan", "Dela", "Cruz"];
console.log(`${firstName} ${middleName} ${lastName}` )

	 
